package com.onetomanyunidirectional.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.onetomanyunidirectional.model.Library;

public interface LibraryRepository extends JpaRepository<Library, Integer>{  
}